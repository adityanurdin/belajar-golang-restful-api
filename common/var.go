package common

var App Application

const (
	SUBS_DEACTIVED = -1
	SUBS_CREATED   = 0
	SUBS_ACTIVATED = 1
)

const (
	LP1 = iota + 1
)

const (
	CP1 = iota + 100
	PORTAL
)

var WEBTYPE = map[int]string{
	LP1:    "lp1",
	CP1:    "cp1",
	PORTAL: "portal",
}
