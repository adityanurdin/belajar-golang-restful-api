package common

import "gorm.io/gorm"

type Application struct {
	Config *Config
	DB *gorm.DB
}