package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gin-gonic/gin"
	"golang.org/x/sync/errgroup"

	core "belajar-gin/common"
)

var g errgroup.Group

func main() {

	if err := core.SetupConfig(); err != nil {
		log.Fatalf("Cannot setup config: %v", err)
	}

	// close db connection
	genericDB, _ := core.App.DB.DB()
	defer genericDB.Close()

	// setup gin mode
	if core.App.Config.Debug {
		gin.SetMode(gin.DebugMode)
	} else {
		gin.SetMode(gin.ReleaseMode)
	}

	server01 := &http.Server{
		Addr:    core.App.Config.HttpAddress["addr1"],
		Handler: router01(),
	}

	g.Go(func() error {
		err := server01.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			log.Fatalf("Cannot run webserver: %v", err)
		}	

		log.Printf("Server running at %s", server01.Addr)

		quit := make(chan os.Signal, 2)
		signal.Notify(quit, os.Interrupt, syscall.SIGTERM)
		<-quit

		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()

		if err := server01.Shutdown(ctx); err != nil {
			log.Fatalf("Can't shutdown server %v", err)
		}

		log.Println("server exiting")
		
		return err
	})

	if err := g.Wait(); err != nil {
		log.Fatal(err)
	}
}
