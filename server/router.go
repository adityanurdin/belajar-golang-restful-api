package main

import (
	"belajar-gin/middleware"
	"belajar-gin/modules/handler"
	"net/http"

	"github.com/gin-gonic/gin"
)

func router01() http.Handler {

	router := gin.New()

	// Middleware
	router.Use(gin.Recovery())
	router.Use(middleware.Cors)

	APIV1 := router.Group("/api/v1")
	{
		APIPRODUCT := APIV1.Group("/products")
		{
			// retrieve products data
			APIPRODUCT.GET("/", handler.ProductGetAllHandler())
			// get product data by id
			APIPRODUCT.GET("/:id", handler.ProductGetByIdHandler())
			// store product data
			APIPRODUCT.POST("/", handler.ProductCreateHandler())
			// update product data by id
			APIPRODUCT.PUT("/:id", handler.ProductUpdateHandler())
			// delete product data by id
			APIPRODUCT.DELETE("/:id", handler.ProductDeleteHandler())
		}
	}

	return router
}