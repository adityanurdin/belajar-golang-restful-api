## Golang Restful API

---

## How to deploy ?

1. Build Application

- **native** `go build -o golang-server`
- **linux** `env GOOS=linux GOARCH=amd64 go build -o golang-server`

2. Deploy with PM2

- copy `pm2.start.example.json` to `pm2.start.json`
- edit config with your own config <br>

example

```
"apps": [
    {
      "name": "golang-restful-api-service",
      "script": "./golang-server -config='config.json' -debug=true"
    }
  ]
```

- run `pm2 start pm2.start.json --watch`

---

<br><br>

## API SPEC

BASE_URL = `http://your_server_api/api/v1/`

## Products <br><br>

### Get All Products<br>

example

```
content-type: application/json

method: GET
example endpoint: http://127.0.0.1:8080/api/v1/products
```

response

```
{
    data: [
        {
            id: 1,
            name: "Macbook Pro 2021",
            created_at: "2022-02-16T02:03:35.005661+07:00",
            updated_at: "2022-02-16T02:03:35.005661+07:00"
        },
        {
            id: 2,
            name: "Macbook Pro 2020",
            created_at: "2022-02-16T03:03:38.992194+07:00",
            updated_at: "2022-02-16T03:03:38.992194+07:00"
        }
    ],
    message: "Successfully retrieve data products",
    status: true
}
```

### Get Product By ID<br>

example

```
content-type: application/json

method: GET
example endpoint: http://127.0.0.1:8080/api/v1/products/1
```

response

```
{
    data: {
            id: 1,
            name: "Macbook Pro 2021",
            created_at: "2022-02-16T02:03:35.005661+07:00",
            updated_at: "2022-02-16T02:03:35.005661+07:00"
        },
    message: "Successfully get data product by id",
    status: true
}
```

### Add Product<br>

example

```

content-type: application/json

method: POST
example endpoint: http://127.0.0.1:8080/api/v1/products
example request body:
{
    name: "product_name",
}

```

response

```
{
    "data": {
        "id": 1,
        "name": "Macbook Pro 2021",
        "created_at": "2022-02-16T03:38:34.241862+07:00",
        "updated_at": "2022-02-16T03:38:34.241862+07:00"
    },
    "message": "Successfully store data product",
    "status": true
}
```

### Update Product <br>

example

```

content-type: application/json

method: PUT
example endpoint: http://127.0.0.1:8080/api/v1/products/1
example request body:
{
    name: "new_product_name",
}

```

response

```
{
    "data": {
        "id": 1,
        "name": "Macbook Pro 2015",
        "created_at": "2022-02-16T03:38:34.241862+07:00",
        "updated_at": "2022-02-16T03:40:43.3095+07:00"
    },
    "message": "Successfully update data product by id",
    "status": true
}
```

### Delete Product <br>

example

```
content-type: application/json

method: DELETE
example endpoint: http://127.0.0.1:8080/api/v1/products/1
```

response

```
{
    "message": "Successfully delete data product by id",
    "status": true
}
```
