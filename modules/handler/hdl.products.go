package handler

import (
	"belajar-gin/modules/inputs"
	"belajar-gin/modules/models"
	"belajar-gin/modules/tables"
	"net/http"

	"github.com/gin-gonic/gin"

	core "belajar-gin/common"
)

func ProductGetAllHandler() gin.HandlerFunc {
	return func(c *gin.Context) {

		db := core.App.DB

		productModel := models.NewProductModel(db)
		products, err := productModel.GetAll([]tables.Products{})

		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"status": false,
				"message": "Error while retrieve data products",
				"error": err.Error(),
			})

			return
		}

		c.JSON(http.StatusOK, gin.H{
			"status":  true,
			"message": "Successfully retrieve data products",
			"data": products,
		}) 
	}
}

func ProductGetByIdHandler() gin.HandlerFunc {
	return func(c *gin.Context) {

		product_id := c.Param("id")

		productModel := models.NewProductModel(core.App.DB)
		product, err := productModel.GetById(tables.Products{}, product_id)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"status": false,
				"message": "Error while get data product by id",
				"error": err.Error(),
			})
			return
		}

		c.JSON(http.StatusOK, gin.H{
			"status":  true,
			"message": "Successfully get data product by id",
			"data":    product,
		})

	}
}

func ProductCreateHandler() gin.HandlerFunc {
	return func(c *gin.Context) {

		var input inputs.ProductInput

		err := c.ShouldBindJSON(&input)
		if err != nil {
			c.JSON(http.StatusBadRequest, nil)
		}

		productModel := models.NewProductModel(core.App.DB)
		product := tables.Products{
			Name: input.Name,
		}

		product, err = productModel.Save(product)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"status": false,
				"message": "Error while store data product",
				"error": err.Error(),
			})

			return
		}

		c.JSON(http.StatusOK, gin.H{
			"status":  true,
			"message": "Successfully store data product",
			"data":    product,
		})

	}
}

func ProductUpdateHandler() gin.HandlerFunc {
	return func(c *gin.Context) {

		var input inputs.ProductInput

		err := c.ShouldBindJSON(&input)
		if err != nil {
			c.JSON(http.StatusBadRequest, nil)
		}

		product_id := c.Param("id")

		productModel := models.NewProductModel(core.App.DB)
		product, err := productModel.GetById(tables.Products{}, product_id)
		if err != nil {
			c.JSON(http.StatusNotFound, gin.H{
				"status": false,
				"message": "Error while update data product by id",
				"error": err.Error(),
			})
			return
		}

		product.Name = input.Name
		updateProduct, err := productModel.Update(product)

		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"status": false,
				"message": "Error while update data product by id",
				"error": err.Error(),
			})

			return
		}

		c.JSON(http.StatusOK, gin.H{
			"status":  true,
			"message": "Successfully update data product by id",
			"data":    updateProduct,
		})

	}
}

func ProductDeleteHandler() gin.HandlerFunc {
	return func(c *gin.Context) {
		product_id := c.Param("id")

		productModel := models.NewProductModel(core.App.DB)
		_, err := productModel.Delete(tables.Products{}, product_id)
		if err != nil {
			c.JSON(http.StatusInternalServerError, gin.H{
				"status": false,
				"message": "Error while delete data product by id",
				"error": err.Error(),
			})

			return
		}

		c.JSON(http.StatusOK, gin.H{
			"status":  true,
			"message": "Successfully delete data product by id",
		})
	}
}