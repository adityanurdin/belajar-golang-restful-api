package tables

import "time"

type Products struct {
	 ID uint `gorm:"primaykey" json:"id"`
	 Name string `gorm:"type:varchar(100)" json:"name"`
	 CreatedAt time.Time `json:"created_at"`
	 UpdatedAt time.Time `json:"updated_at"`
}