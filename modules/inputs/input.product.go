package inputs

type ProductInput struct {
	Name string `json:"name" binding:"required"`
}