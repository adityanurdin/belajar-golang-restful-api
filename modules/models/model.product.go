package models

import (
	"belajar-gin/modules/tables"

	"gorm.io/gorm"
)

type ProductModel interface {
	GetAll(products []tables.Products, id string) ([]tables.Products, error)
	GetById(product tables.Products) (tables.Products, error)
	Save(product tables.Products) (tables.Products, error)
	Update(product tables.Products) (tables.Products, error)
	Delete(product tables.Products, id string) (tables.Products, error)
}

type productModel struct {
	db *gorm.DB
}

func NewProductModel(db *gorm.DB) *productModel {
	return &productModel{db}
}

func (r *productModel) GetAll(products []tables.Products) ([]tables.Products, error) {

	err := r.db.Find(&products).Error
	if err != nil {
		return products, err
	}

	return products, nil
	
}

func (r *productModel) GetById(product tables.Products, id string) (tables.Products, error) {
	err := r.db.First(&product, id).Error

	if err != nil {
		return product, err
	}

	return product, nil
}

func (r *productModel) Save(product tables.Products) (tables.Products, error) {

	err := r.db.Create(&product).Error
	if err != nil {
		return product, err
	}

	return product, nil

}

func (r *productModel) Update(product tables.Products) (tables.Products, error) {
	err := r.db.Save(&product).Error

	if err != nil {
		return product, err
	}

	return product, nil
}

func (r *productModel) Delete(product tables.Products, id string) (tables.Products, error) {
	err := r.db.Delete(&product, id).Error

	if err != nil {
		return product, err
	}

	return product, nil
}

